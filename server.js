const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
var cors = require("cors");
const movie = require('./routes/api/movie');
const genres = require('./routes/api/genres');

const app = express();
app.use(cors());
const port = process.env.PORT || 5000;

var server = app.listen(port);
// Body parser middleware
app.use(express.json({ extended: false }));

const db =
"mongodb+srv://admin:admin@cluster0.zwo1d.mongodb.net/cluster0?retryWrites=true&w=majority";

// Connect to MongoDB
mongoose
  .connect(db)
  .then(() => {
    console.log('MongoDB Connected')
  })
  .catch(err => console.log(err));
// Use Routes
app.use("/api/genre", genres);
app.use("/api/movie", movie);



app.get("/", (req, res) => {
  console.log("hello");
  res.send("hello");
});

