const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MoiveSchema = mongoose.Schema({
  genreId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "genres",
  },
  name: { type: String, required: true },
  description: { type: String, required: true },
  duration: { type: String, required: true },
  rating:{ type: String, required: true },
  date: {
    type: Date,
    default: Date.now
  }

});

module.exports = mongoose.model("Movie", MoiveSchema);
