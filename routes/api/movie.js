const express = require('express');
const router = express.Router();
const movieSchema = require("../../models/movie");


// @route    post api/movies
// @desc     add  new movies
// @access   for now public

router.post('/', async (req, res) => {
  try {
      const movie = new movieSchema({
        genreId: req.body.genreId,
        name: req.body.name,
        description:req.body.description,
        duration:req.body.duration,
        rating:req.body.rating
  })
  await movie.save()
  res.send({msg:"movie added"})
  } catch (e) {
    console.log(e)
    res.send('error');
  }
  
});


// @route    get api/movies
// @desc     get  all movies
// @access   for now public

router.get('/', async (req, res) => {
  try {
    let movies = await movieSchema.find().limit(20).populate("genreId",['name','description']);     
    res.json({ movies:movies });
 
  } catch (e) {
    console.log(e)
    res.send('error');
  }
  
});


// @route    post api/genres/delete/:id
// @desc     delete  genres base on id 
// @access   for now public
router.delete("/delete/:id",  async (req, res) => {
  try {
    const movie = await movieSchema.findById(req.params.id);
    
    if (!movie) {
      return res.status(404).json({ msg: "movie not Found" });
    }
    await movie.remove();
    res.json({ msg: "movie removed" });
  } catch (err) {
    console.error(err.message);

    res.status(500).send("Server Error");
  }
});

module.exports = router;
