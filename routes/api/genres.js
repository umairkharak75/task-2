const express = require("express");
const router = express.Router();
const GenresSchema= require("../../models/genres")



// @route    post api/genres
// @desc     add  new genres
// @access   for now public

router.post('/', async (req, res) => {
  console.log(req.body)
  try {
      const createGenres = new GenresSchema({
      name: req.body.name,
      description: req.body.description,
  })
  await createGenres.save()
  res.send({msg:'genres added successfully'})

  } catch (e) {
    console.log(e)
    res.send('error');
  }
  
});
// @route    post api/genres
// @desc     get all genres
// @access   for now public

router.get('/', async (req, res) => {
  try {
    let genres = await GenresSchema.find().limit(20);     
    res.json({ genres:genres });

  } catch (e) {
    console.log(e)
    res.send('error');
  }
  
});

// @route    post api/genres/delete/:id
// @desc     delete  genres base on id 
// @access   for now public
router.delete("/delete/:id", async (req, res) => {
  try {
    const genres = await GenresSchema.findById(req.params.id);
    
    if (!genres) {
      return res.status(404).json({ msg: "genres not Found" });
    }
    await genres.remove();
    res.json({ msg: "Genres removed" });
  } catch (err) {
    console.error(err.message);

    res.status(500).send("Server Error");
  }
});

module.exports = router;



